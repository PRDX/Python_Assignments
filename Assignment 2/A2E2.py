# Assignment:2 Exercise:2
import csv

with open('wk2_test1_in.csv', newline='') as inFile:
    reader = csv.reader(inFile)

    with open('wk2_test1_out.csv', 'w') as outFile:
        writer = csv.writer(outFile)

        for row in reader:
            wordStep = 0
            for x in row:
                if len(x) < 10:
                    for i in range(len(x), 10):
                        x = x + " "
                elif len(x) > 15:
                        x = x[:15]
                row[wordStep] = x
                wordStep += 1
            writer.writerow(row)
