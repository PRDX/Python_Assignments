# Assignment:2 Exercise:1

class List_Mean():
    the_list = []

    def set_list(*args):
        List_Mean.the_list = []
        step = 0
        for x in args[1:]:
            List_Mean.the_list.insert(step, x)
            step += 1

    def get_list(self):
        return self.the_list

    def calc_avg(self, some_list):
        avg = 0
        step = 0
        for x in some_list:
            if isinstance(x, str):
                continue
            avg += x
            step += 1
        avg /= step
        return avg


def prnt_avg(my_obj):
    a_list = my_obj.get_list()
    average = my_obj.calc_avg(a_list)
    print(average)


my_obj = List_Mean()

my_obj.set_list(1, 2, 17, 52.5, 43, 4.4, 10)
prnt_avg(my_obj)

my_obj.set_list(1, 2, 3, 4, 5, 6, 7, "dog")
prnt_avg(my_obj)
