# Assignment:2 Exercise:3 Part:2

with open('wk2_test2_in.log', 'r') as inFile:

    maxMGCs = [0, 0, 0, 0, 0, 0, 0, 0, 0] #[time took, date, time]x3
    avgMinor = [0, 0] #[time sum, GCs instances]
    maxFGCs = [0, 0, 0, 0, 0, 0, 0, 0, 0]
    avgFull = [0, 0]

    step = 0
    for line in inFile:
        log = line.split()
        time = log[0].split("T")

        #Minor GC
        if log[2] == '[GC':
            if float(log[4]) > float(maxMGCs[0]):
                #make 2nd into 3rd
                maxMGCs[6] = maxMGCs[3]
                maxMGCs[7] = maxMGCs[4]
                maxMGCs[8] = maxMGCs[5]
                #made 1st into 2nd
                maxMGCs[3] = maxMGCs[0]
                maxMGCs[4] = maxMGCs[1]
                maxMGCs[5] = maxMGCs[2]
                #new max
                maxMGCs[0] = log[4]
                maxMGCs[1] = time[0]
                maxMGCs[2] = time[1][:12]
            avgMinor[0] += float(log[4])
            avgMinor[1] += 1

        #Full GC
        if log[2] == '[Full':
            if float(log[5]) > float(maxFGCs[0]):
                # make 2nd into 3rd
                maxFGCs[6] = maxFGCs[3]
                maxFGCs[7] = maxFGCs[4]
                maxFGCs[8] = maxFGCs[5]
                # made 1st into 2nd
                maxFGCs[3] = maxFGCs[0]
                maxFGCs[4] = maxFGCs[1]
                maxFGCs[5] = maxFGCs[2]
                # new max
                maxFGCs[0] = log[5]
                maxFGCs[1] = time[0]
                maxFGCs[2] = time[1][:12]
            avgFull[0] += float(log[5])
            avgFull[1] += 1

        step += 1

    print("The average time for a Minor GC is %f" % (avgMinor[0]/avgMinor[1]))
    print("The average time for a Full GC is %f" % (avgFull[0]/avgFull[1]))
    print("The 3 top minor GCs that took the longest:")
    print("Datetime:%s %s Time took: %s" % (maxMGCs[1], maxMGCs[2], maxMGCs[0]))
    print("Datetime:%s %s Time took: %s" % (maxMGCs[4], maxMGCs[5], maxMGCs[3]))
    print("Datetime:%s %s Time took: %s" % (maxMGCs[7], maxMGCs[8], maxMGCs[6]))
    print("The 3 top Full GCs that took the longest:")
    print("Datetime:%s %s Time took: %s" % (maxFGCs[1], maxFGCs[2], maxFGCs[0]))
    print("Datetime:%s %s Time took: %s" % (maxFGCs[4], maxFGCs[5], maxFGCs[3]))
    print("Datetime:%s %s Time took: %s" % (maxFGCs[7], maxFGCs[8], maxFGCs[6]))
