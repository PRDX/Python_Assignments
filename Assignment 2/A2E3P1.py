# Assignment:2 Exercise:3 Part:1

with open('wk2_test2_in.log', 'r') as inFile:

    timeperiod = [0, 0, 0, 0]
    GCs = [0, 0]

    step = 0
    for line in inFile:
        log = line.split()

        #Time Period
        time = log[0].split("T")
        if step == 0:
            timeperiod[0] = time[0]
            timeperiod[1] = time[1]
        else:
            timeperiod[2] = time[0]
            timeperiod[3] = time[1]

        #Garbadge Collectors
        if log[2] == '[GC':
            GCs[0] += 1
        elif log[2] == '[Full':
            GCs[1] += 1

        step += 1

    print("The .log starts at %s %s and end at %s %s" % (timeperiod[0], timeperiod[1][:12], timeperiod[2], timeperiod[3][:12]))
    print("Number of minor GCs: %s" % GCs[0])
    print("Number of full GCs: %s" % GCs[1])
