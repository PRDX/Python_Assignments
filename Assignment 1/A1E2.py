# Assignment:1 Exercise:2

food = ['pizza', 'loukoumades', 'dolmades', 'hamburger', 'tzaziki', 'paidakia']

count = [0, 0, 0, 0, 0]

for x in food:
    for y in x:
        if y == "a":
            count[0] += 1
        elif y == "e":
            count[1] += 1
        elif y == "i":
            count[2] += 1
        elif y == "o":
            count[3] += 1
        elif y == "u":
            count[4] += 1

print("The letter A appears %d times." % count[0])
print("The letter E appears %d times." % count[1])
print("The letter I appears %d times." % count[2])
print("The letter O appears %d times." % count[3])
print("The letter U appears %d times." % count[4])

"""
ls = ''.join(food)
letters = ['a', 'e', 'i', 'o', 'u']
for x in xrange(len(letters)):
    print "The letter %s appears %d times." % (str(letters[x]), ls.count(letters[x]))
"""